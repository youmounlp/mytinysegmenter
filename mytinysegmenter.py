import re
import unicodedata

class MyTinySegmenter():
  def __init__(self):
    # 文字種変換
    self._patterns = {
      # 数字、漢数字
      "N": "[0-9０-９零一二三四五六七八九十百千万億兆京]", 
      # ひらがな
      "I": "[ぁ-ん]", 
      # カタカナ
      "K": "[ァ-ヴｱ-ﾝﾞ]", 
      #アルファベット
      "A": "[a-zA-Zａ-ｚＡ-Ｚ]",
      # CJK統合漢字
      "H": "[一-鿯]", 
      # 記号
      "P": "[ー、-〜!-/:-@[-`{-~]" 
    }
    
    self._chartypes = []
    for ctype, ptn in self._patterns.items():
      self._chartypes.append(
        [re.compile(ptn), ctype]
      )

    # 前処理特徴量のカラム名
    c1 = ["c{}".format(i) for i in range(1,7)]
    t1 = ["t{}".format(i) for i in range(1,7)]
    c2 = ["c{}{}".format(i, i+1) for i in range(1,6)]
    t2 = ["t{}{}".format(i, i+1) for i in range(1,6)]
    self._feature_columns = c1 + t1 + c2 + t2

    # L1正則化線形モデルで学習した重み(x1000)
    # 最初は空
    self._weights = {
			'c1': {'、': -115 ,'い': 52 ,'が': -272 ,'く': -69 ,'こ': 197 ,'す': 98 ,
				'っ': 152 ,'つ': -63 ,'て': -60 ,'で': -87 ,'と': -147 ,'な': 76 ,
				'に': -57 ,'の': 72 ,'は': -261 ,'ひ': 61 ,'ぶ': -515 ,'む': 304 ,
				'も': -145 ,'る': 131 ,'を': -148 ,'ロ': 1736 ,'ン': -16 ,'ー': 427 ,
				'一': -159 ,'住': 188 ,'受': -1145 ,'好': 17 ,'嫌': -392 ,'悔': 1373 ,
				'物': -430 ,'百': -26 ,'皆': 231 ,'蔵': 1205 ,'走': 473 ,},
			'c2': {'0': 58 ,']': -177 ,'^': -197 ,'k': 565 ,'、': -220 ,'々': 421 ,
				'」': 1467 ,'う': 46 ,'が': -364 ,'こ': 430 ,'せ': 3189 ,'そ': -636 ,
				'た': 206 ,'て': -145 ,'で': -236 ,'と': -283 ,'な': -36 ,'に': -443 ,
				'の': 140 ,'は': -347 ,'ば': -201 ,'ま': 321 ,'も': -132 ,'よ': 739 ,
				'り': -182 ,'を': -375 ,'ん': 192 ,'ッ': 34 ,'ー': 721 ,'不': -973 ,
				'中': -29 ,'交': 741 ,'今': -252 ,'使': 56 ,'便': -300 ,'午': 1780 ,
				'卒': 1802 ,'参': 1352 ,'夏': -313 ,'外': 235 ,'大': -124 ,'好': -617 ,
				'庫': 1963 ,'必': -499 ,'態': 915 ,'択': -318 ,'抹': 426 ,'携': 1575 ,
				'旅': 590 ,'最': -356 ,'本': -271 ,'歴': 1929 ,'気': -603 ,'清': 748 ,
				'牛': -1414 ,'百': -2464 ,'皇': 1814 ,'目': -464 ,'確': -446 ,'簡': -257 ,
				'結': 612 ,'落': -3182 ,'行': 1415 ,'見': -286 ,'雰': -1586 ,'電': 7519 ,
				'非': -909 ,'高': 310 ,},
			'c3': {'.': -687 ,'1': 155 ,'3': 103 ,'[': 2850 ,']': 2355 ,'m': 345 ,
				'、': 4213 ,'。': -21 ,'々': -2084 ,'「': 2367 ,'」': 22 ,'あ': -3203 ,
				'い': -69 ,'が': 1958 ,'く': 323 ,'け': -210 ,'こ': -1491 ,'ご': -2251 ,
				'し': 42 ,'す': -113 ,'そ': -2056 ,'た': 91 ,'ち': -276 ,'っ': -8413 ,
				'て': 5183 ,'で': 1648 ,'と': 850 ,'に': 2880 ,'ね': -16 ,'の': 1253 ,
				'は': 2087 ,'ば': 84 ,'ひ': -1859 ,'ほ': -1651 ,'ま': -2054 ,'み': -45 ,
				'も': 843 ,'ゃ': 138 ,'よ': -1464 ,'る': 2046 ,'れ': 189 ,'を': 13930 ,
				'ん': -396 ,'カ': -76 ,'ッ': -635 ,'ー': -1056 ,'一': -1693 ,'交': -4927 ,
				'京': -2010 ,'人': 894 ,'今': 426 ,'休': -169 ,'使': -1365 ,'僕': 2134 ,
				'光': 159 ,'全': -359 ,'出': -310 ,'利': -958 ,'前': 430 ,'勉': -552 ,
				'単': -634 ,'厳': -756 ,'史': 1929 ,'名': -594 ,'囲': -1352 ,'国': 124 ,
				'夏': 1752 ,'夜': 2324 ,'夢': 394 ,'大': -151 ,'子': 427 ,'学': 130 ,
				'実': -463 ,'寺': 848 ,'小': -609 ,'少': -6134 ,'川': 909 ,'市': 1036 ,
				'帯': 2066 ,'年': 690 ,'店': 727 ,'度': 516 ,'当': -1942 ,'携': -115 ,
				'数': 1201 ,'文': -177 ,'方': 226 ,'本': 709 ,'業': 206 ,'楽': -507 ,
				'機': -337 ,'水': 711 ,'物': 839 ,'特': -883 ,'町': 171 ,'的': -1773 ,
				'知': -553 ,'私': 2714 ,'自': -380 ,'若': -1960 ,'茶': 4051 ,'要': -336 ,
				'見': 156 ,'話': 139 ,'誰': 1034 ,'買': -2281 ,'足': 188 ,'車': 785 ,
				'近': 350 ,'逃': -512 ,'通': -262 ,'道': 197 ,'間': 997 ,'電': -486 ,
				'頼': -2043 ,},
			'c4': {'!': 1278 ,'0': -8510 ,'J': 1919 ,'N': 494 ,']': 720 ,'^': -2092 ,
				'c': -705 ,'t': -1480 ,'、': 403 ,'。': 302 ,'々': -7916 ,'」': 3626 ,
				'〜': -6358 ,'ぁ': -193 ,'あ': 1714 ,'い': -514 ,'う': -254 ,'え': -5542 ,
				'お': 3984 ,'か': 336 ,'が': 2356 ,'き': -1638 ,'く': -2566 ,'け': -381 ,
				'げ': -27 ,'こ': 518 ,'さ': 845 ,'し': 462 ,'ず': 239 ,'そ': 1509 ,
				'た': -237 ,'だ': 1000 ,'ち': -545 ,'っ': -317 ,'つ': 32 ,'て': -17351 ,
				'で': 1836 ,'と': 2007 ,'に': 1709 ,'ね': 6186 ,'の': 2449 ,'は': 2512 ,
				'ば': -304 ,'ひ': 705 ,'へ': 2237 ,'べ': -140 ,'ま': 273 ,'め': -158 ,
				'も': 2002 ,'ゃ': -1699 ,'や': 1491 ,'よ': 1814 ,'ら': -3864 ,'り': -24546 ,
				'る': -9624 ,'ろ': -2684 ,'を': 30154 ,'ん': -849 ,'ア': 22 ,'ト': -18 ,
				'ヤ': -987 ,'ル': -989 ,'ー': -22547 ,'中': 518 ,'丸': -1785 ,'人': 178 ,
				'休': 102 ,'使': 1396 ,'側': 1105 ,'光': -1602 ,'出': -1321 ,'創': 1839 ,
				'取': -358 ,'合': -1926 ,'変': 479 ,'字': -302 ,'学': -212 ,'定': -41 ,
				'客': 0 ,'店': 190 ,'思': 221 ,'換': -2080 ,'時': 590 ,'書': 425 ,
				'機': 817 ,'父': 121 ,'物': -2204 ,'的': 1307 ,'社': -898 ,'祭': 1417 ,
				'者': 721 ,'能':-423 ,'話': -145 ,'語': 95 ,'車': -759 ,'込': -2060 ,
				'返': -452 ,'都': -1397 ,'間': -1240 ,'電': 415 ,'首': -1338 ,'駅': 1297 ,
				},
			'c5': {')': -221 ,'>': 2345 ,'?': 606 ,'F': 1467 ,'t': -530 ,'−': 931 ,
				'ぁ': 298 ,'い': -56 ,'お': -135 ,'が': -75 ,'ぎ': 433 ,'け': 196 ,
				'げ': -2982 ,'さ': -503 ,'し': -96 ,'じ': 195 ,'す': -129 ,'せ': 392 ,
				'た': 264 ,'っ': 299 ,'つ': 399 ,'て': 25 ,'で': -127 ,'と': -77 ,
				'ど': 173 ,'な': -149 ,'に': -168 ,'の': -118 ,'は': -184 ,'べ': 453 ,
				'も': -45 ,'ょ': 2378 ,'り': -52 ,'れ': 302 ,'を': -83 ,'ラ': 195 ,
				'ー': 1765 ,'前': -364 ,'寺': -1122 ,'気': -177 ,'的': -646 ,'社': 675 ,
				'能': 719 ,'行': 338 ,'見': 135 ,'角': 1940 ,'話': 445 ,'遮': 643 ,
				'電': -171 ,'首': -364 ,},
			'c6': {'/': -493 ,'P': 1387 ,']': 2192 ,'_': 100 ,'m': -830 ,'p': -1621 ,
				'、': -45 ,'。': -125 ,'お': -38 ,'か': 98 ,'こ': -407 ,'ず': -352 ,
				'せ': -205 ,'ぜ': 695 ,'ど': -38 ,'な': 71 ,'に': 109 ,'ぱ': 40 ,
				'も': -82 ,'ら': 45 ,'ん': 307 ,'キ': -2602 ,'ピ': 207 ,'以': 500 ,
				'動': -497 ,'学': -261 ,'思': 48 ,'断': 3011 ,'直': 1496 ,'違': -1903 ,
				'首': -209 ,},
			't1': {'K': 112 ,'O': 115 ,'P': -302 ,},
			't2': {'H': 230 ,'K': 174 ,'N': 313 ,},
			't3': {'A': -1250 ,'I': 4983 ,'O': 437 ,},
			't4': {'H': 630 ,'O': 540 ,},
			't5': {'A': 25 ,'N': 60 ,},
			't6': {'H': -31 ,'I': 114 ,'K': 71 ,},
			'c12': {'0k': 1052 ,'Dを': -1884 ,'IM': -286 ,'[携': 1576 ,'_し': -1001 ,'_歴': 1929 ,
				'、ダ': -998 ,'ある': -1085 ,'いっ': -12 ,'いも': 224 ,'お抹': 426 ,'かせ': 1628 ,
				'から': -87 ,'か人': 2071 ,'があ': 116 ,'が大': -96 ,'が当': -1207 ,'くそ': 3510 ,
				'くと': 1298 ,'くら': 836 ,'こと': 115 ,'ご覧': 269 ,'され': 2632 ,'すが': 1336 ,
				'すの': 50 ,'たな': -2731 ,'たも': 1228 ,'ち巡': -1314 ,'てい': 1633 ,'ても': -3254 ,
				'て思': 1478 ,'で外': 0 ,'とい': 389 ,'とお': 177 ,'とっ': -214 ,'とバ': 92 ,
				'と思': 95 ,'と牛': -1973 ,'なん': -369 ,'にと': -973 ,'の卒': 1803 ,'は6': -687 ,
				'はそ': -516 ,'びっ': -1318 ,'ぶ・': -2028 ,'ませ': 547 ,'みた': -185 ,'みよ': 660 ,
				'むの': 896 ,'もす': 10 ,'も少': -1895 ,'も雰': -1599 ,'よう': 565 ,'らし': 1019 ,
				'ら誰': -478 ,'ら電': 634 ,'れた': -100 ,'ろが': -1480 ,'を利': 137 ,'ェが': -72 ,
				'ラン': 858 ,'ロー': 417 ,'・・': 8 ,'万能': 73 ,'上皇': 1823 ,'住ん': 1687 ,
				'作り': -101 ,'受験': -568 ,'嫌を': -2020 ,'家庭': -155 ,'帯「': -1453 ,'思う': -219 ,
				'悔し': 1373 ,'状態': 1436 ,'百人': -1533 ,'皆言': 1616 ,'立っ': 1567 ,'立は': -512 ,
				'蔵庫': 1963 ,'行っ': 3859 ,'走っ': 1111 ,'遠い': -613 ,'選択': -318 ,'閣に': -384 ,
				},
			'c23': {'.ブ': 1256 ,'6年': -1051 ,'Mカ': -286 ,'_[': 3356 ,'_h': -1621 ,'_「': 887 ,
				'_お': 524 ,'_だ': -835 ,'_で': -959 ,'_も': -1949 ,'_足': 1355 ,'km': 1387 ,
				'n.': -921 ,'、お': 72 ,'、か': -2614 ,'、と': 1816 ,'、厳': -466 ,'、食': 223 ,
				'々な': 156 ,'「ブ': -1453 ,'いい': 1469 ,'いけ': -2681 ,'いた': 1234 ,'いつ': -1393 ,
				'いも': -1154 ,'いん': -1606 ,'うの': -709 ,'えら': -82 ,'かも': 437 ,'から': 1585 ,
				'が、': 741 ,'が使': -1482 ,'が夢': 1587 ,'が沢': -76 ,'ここ': 1397 ,'こと': 1373 ,
				'この': -1118 ,'これ': 6130 ,'さん': 911 ,'しい': 4363 ,'した': 875 ,'して': 98 ,
				'せか': 655 ,'そこ': 5838 ,'それ': 1173 ,'そん': -11680 ,'たち': 5614 ,'たと': -1176 ,
				'たの': -291 ,'だけ': 425 ,'っく': -141 ,'った': 1515 ,'てく': -647 ,'てし': -3024 ,
				'ても': 2755 ,'でき': 209 ,'です': 3319 ,'とい': -366 ,'とか': 900 ,'とき': 713 ,
				'とっ': -973 ,'とて': -3313 ,'どこ': 2430 ,'どん': 427 ,'ない': 6092 ,'など': 374 ,
				'なも': -3255 ,'なり': 3141 ,'につ': -3540 ,'のか': 723 ,'のた': -5390 ,'のに': 1919 ,
				'のは': 269 ,'の中': 3578 ,'の人': 24 ,'はう': -1838 ,'は逃': -512 ,'まし': -2808 ,
				'ます': 79 ,'ませ': 2082 ,'まで': 1695 ,'まま': 813 ,'みな': -109 ,'もう': 3756 ,
				'もの': 9999 ,'もん': 619 ,'も通': -1985 ,'も頼': -1647 ,'よう': -1613 ,'よく': 86 ,
				'より': 159 ,'よる': 1332 ,'ら、': 779 ,'らい': 348 ,'ら少': -902 ,'るこ': -9337 ,
				'ると': 1024 ,'れて': 3095 ,'れな': -848 ,'れば': 1077 ,'わら': 200 ,'をそ':-1653 ,
				'を買': -1881 ,'んで': 2076 ,'んと': -1102 ,'グを': 1990 ,'ダイ': -987 ,'ツ]': 1357 ,
				'ー−': 1536 ,'ード': 1577 ,'交通': 1165 ,'京都': 442 ,'人一': -1533 ,'人気': 2067 ,
				'今で': -623 ,'使わ': 57 ,'便利': -823 ,'利用': 583 ,'卒業': 1802 ,'夏休': -313 ,
				'外国': 1777 ,'好き': -1055 ,'寺行': -79 ,'少な': -1970 ,'巡っ': -1334 ,'庫の': 1965 ,
				'庭料': -155 ,'当た': -1189 ,'思い': -375 ,'思っ': 2018 ,'態が': 1454 ,'択す': -1733 ,
				'抹茶': 426 ,'携帯': 1575 ,'旅行': 342 ,'日本': 237 ,'普通': -1059 ,'最後': -66 ,
				'最近': 635 ,'様々': -1667 ,'歴史': 1929 ,'気に': -752 ,'牛若': -1973 ,'百人': -364 ,
				'皇が': 1822 ,'簡単': -257 ,'覧に': 269 ,'観光': 1376 ,'言う': 1863 ,'誰で': -469 ,
				'都観': -2353 ,'間あ': -1973 ,'間が': 1281 ,'雰囲': -1586 ,'電波': 621 ,'験勉': -568 ,
				},
			'c34': {'!!': -5740 ,'..': 306 ,'.c': -913 ,'00': -2380 ,'10': -1574 ,']父': 1378 ,
				'ht': -1621 ,'−−': 978 ,'、N': 765 ,'、、': -3666 ,'、ち': 779 ,'。。': -2774 ,
				'々な': -1265 ,'あか': -524 ,'ある': -1948 ,'いこ': 26 ,'いち': -3497 ,'いて': -2897 ,
				'いな': 255 ,'いん': 2431 ,'うか': 1836 ,'うっ': -864 ,'うで': -1001 ,'うと': 916 ,
				'うに': -992 ,'うの': 991 ,'お寺': 1025 ,'か?': 1694 ,'かと': 4791 ,'かに': -1320 ,
				'かも': -2785 ,'がか': 1617 ,'がち': 62 ,'がっ': -2017 ,'がら': -1219 ,'が創': 1847 ,
				'くさ': -885 ,'くな': 551 ,'くり': -1315 ,'こか': 1741 ,'こで': 2827 ,'こと': -11484 ,
				'この': -829 ,'され': 7085 ,'しか': -779 ,'する': -15297 ,'せん': 2056 ,'そこ': -1649 ,
				'その': -574 ,'たい': -8044 ,'たく': -5201 ,'たの': 9482 ,'ため': -9506 ,'たり': -3663 ,
				'たん': 1115 ,'だっ': -1745 ,'だと': 2755 ,'ちに': 643 ,'って': -6249 ,'つく': -1402 ,
				'てい': 14267 ,'ても': -5160 ,'てる': 6579 ,'であ': -6014 ,'でい': 4298 ,'でし': -4627 ,
				'です': -4157 ,'では': -262 ,'でも': -4319 ,'でる': 5861 ,'とい': 4534 ,'とか': 1743 ,
				'とき': -274 ,'とこ': -3238 ,'とも': -2164 ,'どん': -1383 ,'なぁ': -216 ,'ない': -8721 ,
				'なか': -467 ,'なが': -4345 ,'なに': -1854 ,'なん': 796 ,'にJ': 1931 ,'にな': 7694 ,
				'のか': 3291 ,'のだ': -2725 ,'ので': -10858 ,'のに': -2316 ,'のは': 177 ,'のよ': -659 ,
				'の一': 1939 ,'の事': 1706 ,'の前': -999 ,'の後': -1482 ,'はコ': 901 ,'ひと': -362 ,
				'まし': -877 ,'まで': -269 ,'まれ': 3598 ,'も、': 2056 ,'もい': 3320 ,'もね': 1552 ,
				'もの': -20710 ,'やは': -1887 ,'よう': -847 ,'らし': -404 ,'らな': 5041 ,'らに': -580 ,
				'りし': 649 ,'るだ': 330 ,'れた': -246 ,'れは': 4624 ,'れば': -1633 ,'わか': -3646 ,
				'われ': 1684 ,'を書': 1987 ,'んだ': -940 ,'んで': -1801 ,'んと': -1054 ,'んな': -8887 ,
				'イヤ': -987 ,'カー': -283 ,'スの': 189 ,'ドし': 1671 ,'・・': -5814 ,'一度': 2091 ,
				'一番': -605 ,'一首': -1533 ,'上が': -870 ,'京都': -2912 ,'人が': 469 ,'人一': -360 ,
				'何か': 2058 ,'使わ': -1478 ,'出し': -758 ,'利で': -352 ,'勉強': -552 ,'厳か': -466 ,
				'史も': 1943 ,'囲気': -1586 ,'国人': 1777 ,'夜の': 2995 ,'夢で': 1587 ,'学生': -440 ,
				'少し': -3452 ,'帯電': 1575 ,'常に': -2411 ,'当に': -244 ,'携帯': -115 ,'文字': -150 ,
				'料理': -1053 ,'旅行': -465 ,'日本': -205 ,'最も': -155 ,'業ま': 1803 ,'気な': 1094 ,
				'沢山': -76 ,'波を': 643 ,'用し': 510 ,'百人': -212 ,'的に': -5176 ,'若丸': -1973 ,
				'茶と': 426 ,'茶菓': 182 ,'行き': -75 ,'観光': -2352 ,'買い': -1882 ,'足を': 1352 ,
				'逃げ': -512 ,'通の': -1475 ,'通話': -1969 ,'閣寺': -939 ,'頼れ': -1647 ,'食べ': -393 ,
				'食生': 661 ,},
			'c45': {'..': 1833 ,'/h': 1403 ,'J−': 1919 ,'NF': 765 ,'co': -911 ,'tt': -1621 ,
				'−>': 1505 ,'、、': -1645 ,'、食': 2057 ,'。_': 1863 ,'あり': 11 ,'いい': 5950 ,
				'いけ': 349 ,'いだ': -4170 ,'いど': -2637 ,'いの': -2647 ,'いは': -696 ,'いる': 1072 ,
				'うか': -2707 ,'うど': 398 ,'うな': -847 ,'えて': -2213 ,'か、': 230 ,'か。': 1199 ,
				'かか': 811 ,'かさ': -253 ,'かっ': -5844 ,'かも': 1950 ,'から': 1962 ,'かり': -667 ,
				'かん': -1906 ,'が、': 8524 ,'があ': 481 ,'がっ': -544 ,'がら': -2917 ,'き、': -1452 ,
				'くと': -1401 ,'くら': 2655 ,'けど': 2235 ,'げ出': -512 ,'こと': 13383 ,'こね': -492 ,
				'この': 1320 ,'さい': -742 ,'さに': 3477 ,'さを': 984 ,'さん': -94 ,'し、': 1113 ,
				'しい': -4398 ,'しく': -472 ,'した': 484 ,'して': 735 ,'しな': 757 ,'しま': 1006 ,
				'しょ': -1946 ,'し移': -901 ,'す。': -1229 ,'すぎ': 1086 ,'する': 1679 ,'た。': -25 ,
				'たい': 2180 ,'たく': 2025 ,'たち': 1264 ,'ため': 1477 ,'だけ': 456 ,'だろ': 212 ,
				'ちょ': 12673 ,'った': -693 ,'つつ': 254 ,'ては': -971 ,'てみ': -1305 ,'てる': 3071 ,
				'でん': -249 ,'と、': 1072 ,'とお': 426 ,'とも': -941 ,'とを': -1004 ,'と動': 1632 ,
				'と思': 5012 ,'ど、': -232 ,'どう': 1023 ,'なぁ': 1626 ,'ない': 2584 ,'なお': -4021 ,
				'なか': 809 ,'なが': 385 ,'なく': 1693 ,'なさ': 1411 ,'なっ': 1033 ,'など': 2296 ,
				'なの': 100 ,'なら': 1572 ,'なる': 1189 ,'なん': 2224 ,'な寺': -1761 ,'な言': -2399 ,
				'に、': -261 ,'には': 632 ,'ね。': 2695 ,'のだ': 7047 ,'の乗': 206 ,'は,': 177 ,
				'はり': -1922 ,'は携': 2024 ,'ばっ': 62 ,'ほう': 86 ,'まし': 2066 ,'ます': 5065 ,
				'まっ': -447 ,'まで': 1791 ,'もあ': 2686 ,'もし': -1018 ,'も京': -25 ,'も違': 936 ,
				'よう': -433 ,'らし': 4142 ,'らも': -317 ,'られ': 5496 ,'りな': -1319 ,'り前': -1189 ,
				'るた': -1713 ,'るの': 1888 ,'れば': -818 ,'わか': 180 ,'わけ': 1070 ,'われ': -1471 ,
				'を冷': 1359 ,'を遮': 643 ,'んだ': 996 ,'んで': 1252 ,'んな': -997 ,'メー': 518 ,
				'ヤル': -992 ,'・_': -2028,'ード': -283 ,'一角': 1965 ,'一首': -364 ,'丸の': -1973 ,
				'人旅': 74 ,'休み': 555 ,'創設': 1847 ,'山載': -76 ,'強に': -560 ,'書い': 1354 ,
				'父と': 1364 ,'理の': -193 ,'生活': 700 ,'的に': 380 ,'話機': -1972 ,'通り': 1072 ,
				'都観': -190 ,'間使': -1298 ,'離宮': 100 ,'電話': 1575 ,'首」': -1555 ,},
			'c56': {',安': 179 ,'>a': 1536 ,'Fや': 765 ,'h以': 1403 ,'om': -911 ,'tp': -1621 ,
				'−P': 1919 ,'、7': 1360 ,'、自': -57 ,'」ど': -1555 ,'あま': 36 ,'いと': 31 ,
				'いる': -114 ,'う。': -43 ,'うで': 4028 ,'うと': -1329 ,'お菓': 431 ,'かっ': 859 ,
				'かる': 2100 ,'がら': 1310 ,'が違': -1220 ,'くな': 238 ,'けで': 465 ,'こと': -855 ,
				'さに': -430 ,'さ過': 2080 ,'しれ': -6685 ,'せん': 1014 ,'ため': -901 ,'だ.': 1616 ,
				'だな': 1040 ,'っか': 773 ,'って': -236 ,'てつ': -1838 ,'て一': 548 ,'でに': 1586 ,
				'とが': 4085 ,'とき': -584 ,'とし': -17 ,'と野': 813 ,'な様': -1341 ,'な理': -863 ,
				'に集': -312 ,'ねた': -1246 ,'ので': 124 ,'の代': -536 ,'の身': -529 ,'はキ': -973 ,
				'まし': -254 ,'みた': -526 ,'もあ': -636 ,'もの': -670 ,'ょっ': 58 ,'らい': 2016 ,
				'らメ': 484 ,'りの': -1838 ,'りま': 580 ,'れて': -86 ,'ろい': 306 ,'を大': -1726 ,
				'ドを': -286 ,'ルキ': -994 ,'ンピ': 695 ,'一首': -212 ,'使っ': -1643 ,'冷や': 1359 ,
				'前だ': -1207 ,'動き': 1624 ,'子付': 190 ,'寺の': -976 ,'機能': -97 ,'活を': 568 ,
				'特に': -181 ,'移動': -67 ,'角を': 1965 ,'言う': -466 ,'設さ': 1847 ,'話]': 1576 ,
				'遮断': 643 ,'食事': 805 ,},
			't12': {'HI': 257 ,'IH': 338 ,'II': 207 ,'NA': 701 ,'NH': -109 ,'OO': 329 ,
				'PH': 700 ,'PN': 345 ,},
			't23': {'AA': 866 ,'AK': -284 ,'AP': -560 ,'HH': 572 ,'HP': 580 ,'IH': -302 ,
				'IP': 979 ,'KH': -24 ,'KK': 549 ,'NH': 1582 ,'PA': -6226 ,'PI': -1049 ,
				'PN': 213 ,'PO': 1123 ,},
			't34': {'AA': -9710 ,'AI': 1882 ,'HA': 164 ,'HH': -2334 ,'HI': -1427 ,'HP': 3346 ,
				'IA': 7357 ,'IH': -5166 ,'II': -6554 ,'IK': 1332 ,'IN': 2390 ,'KK': -4519 ,
				'NI': 8787 ,'NN': -13266 ,'OO': -4027 ,'PH': 5258 ,'PK': -2885 ,'PN': 154 ,
				'PP': -579 ,},
			't45': {'HH': 711 ,'HI': 118 ,'II': 50 ,'IK': -281 ,'KH': -6304 ,'KK': 2375 ,
				'KP': 588 ,'OH': 241 ,'PH': 967 ,'PK': -481 ,},
			't56': {'AA': -889 ,'AH': 1499 ,'HH': -178 ,'HP': 1 ,'IK': -72 ,'KI': -1950 ,
				'PH': -115 ,'PP': -143 ,},
			'BIAS': {'0': 237 ,},
    }

  def _ctype(self, char):
    for ptn, ctype in self._chartypes:
      if re.match(ptn, char):
        return ctype
    return "O"

  def _make_feature(self, sentence):
    sentence = unicodedata.normalize("NFKC", "".join(list(sentence)))
    sentence = ["_", "_"] + list(sentence) + ["_", "_"]
    sentence2 = []
    # この文字の前で切れるか
    for i in range(3, len(sentence)-2):
      c1 = sentence[i-3:i+3] # char unigram
      t1 = [self._ctype(c) for c in c1] # char type unigram
      c2 = [c1[i]+c1[i+1] for i in range(len(c1)-1)] # char bigram
      t2 = [t1[i]+t1[i+1] for i in range(len(t1)-1)] # char type bigram
      sentence2 += [c1 + t1 + c2 + t2]
    return sentence2

  def tokenize(self, sentence):
    result = []
    word = sentence[0]

    # 2文字目から最後の文字までの特徴量
    sentence2 = self._make_feature(sentence)

    for i, feature in enumerate(sentence2):
      score = self._weights["BIAS"]["0"]
      for c,f in zip(self._feature_columns, feature):
        if f in self._weights[c].keys():
          score += self._weights[c][f]
      if score > 0:
        # この文字の前で切れる
        result += [word]
        word = sentence[i+1]
      else:
        # この文字の前で切れない
        word += sentence[i+1]
    result += [word]
    return result